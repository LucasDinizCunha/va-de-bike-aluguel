package aluguel.models;

import java.util.UUID;

public class Aluguel {
    private String id;
    private String bicicletaId;
    private String horaInicio;
    private String trancaFimId;
    private String horaFim;
    private String cobrancaId;
    private String ciclistaId;
    private String trancaInicioId;

    public Aluguel (String bicicletaId, String horaInicio, String trancaInicioId, String horaFim, String trancaFimId, String cobrancaId, String ciclistaId) {
        this.bicicletaId = bicicletaId;
        this.horaInicio = horaInicio;
        this.trancaInicioId = trancaInicioId;
        this.horaFim = horaFim;
        this.trancaFimId = trancaFimId;
        this.cobrancaId = cobrancaId;
        this.ciclistaId = ciclistaId;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBicicletaId() {
        return bicicletaId;
    }

    public void setBicicletaId(String bicicletaId) {
        this.bicicletaId = bicicletaId;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getTrancaFimId() {
        return trancaFimId;
    }

    public void setTrancaFimId(String trancaFimId) {
        this.trancaFimId = trancaFimId;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    public String getCobrancaId() {
        return cobrancaId;
    }

    public void setCobrancaId(String cobrancaId) {
        this.cobrancaId = cobrancaId;
    }

    public String getCiclistaId() {
        return ciclistaId;
    }

    public void setCiclistaId(String ciclistaId) {
        this.ciclistaId = ciclistaId;
    }

    public String getTrancaInicioId() {
        return trancaInicioId;
    }

    public void setTrancaInicioId(String trancaInicioId) {
        this.trancaInicioId = trancaInicioId;
    }
}
