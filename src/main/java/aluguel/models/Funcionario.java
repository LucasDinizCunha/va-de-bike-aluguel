package aluguel.models;

import java.util.UUID;

public class Funcionario {
    private String matricula;
    private String senha;
    private String email;
    private String nome;
    private int idade;
    private String funcao;
    private String cpf;

    public Funcionario(String senha, String email, String nome, int idade, String funcao, String cpf) {
        this.matricula = UUID.randomUUID().toString();
        this.senha = senha;
        this.email = email;
        this.nome = nome;
        this.idade = idade;
        this.funcao = funcao;
        this.cpf = cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    public String getMatricula() {
        return matricula;
    }
}

