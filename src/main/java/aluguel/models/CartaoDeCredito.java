package aluguel.models;

public class CartaoDeCredito {
    private String nomeTitular;
    private String numero;
    private String validade;
    private String cvv;

    public CartaoDeCredito(String nomeTitular, String numero, String validade, String cvv) {
        this.nomeTitular = nomeTitular;
        this.numero = numero;
        this.validade = validade;
        this.cvv = cvv;
    }

    public String getNumero() {
        return numero;
    }

    public String getCvv() {
        return cvv;
    }

    public String getValidade() {
        return validade;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public void setNomeTitular(String nomeTitular) {
        this.nomeTitular = nomeTitular;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }
}
