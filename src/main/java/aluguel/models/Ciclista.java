package aluguel.models;

import java.util.UUID;

public class Ciclista {
    private String id;
    private String nome;
    private String nascimento;
    private String cpf;
    private String nacionalidade;
    private String email;
    private String senha;
    private String status;
    private Passaporte passaporte;
    private CartaoDeCredito cartaoDeCredito;

    public Ciclista(String nome, String nascimento, String cpf, String nacionalidade, String email, String senha, Passaporte passaporte, CartaoDeCredito cartaoDeCredito, StatusCiclista status) {
        this.nome = nome;
        this.nascimento = nascimento;
        this.cpf = cpf;
        this.nacionalidade = nacionalidade;
        this.email = email;
        this.senha = senha;
        this.cartaoDeCredito = cartaoDeCredito;
        this.passaporte = passaporte;
        this.status = status.toString();
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public Passaporte getPassaporte() {
        return passaporte;
    }

    public String getStatus() {
        return status;
    }

    public String getEmail() {
        return email;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setStatus(StatusCiclista status) {
        this.status = status.toString();
    }

    public CartaoDeCredito getCartaoDeCredito() {
        return cartaoDeCredito;
    }
}
