package aluguel.models;

public class Passaporte {
    private String numero;
    private String validade;
    private String pais;

    public Passaporte(String numero, String validade, String pais) {
        this.numero = numero;
        this.validade = validade;
        this.pais = pais;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
