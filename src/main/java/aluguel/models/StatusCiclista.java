package aluguel.models;

public enum StatusCiclista {
    ATIVO, INATIVO, AGUARDANDO_CONFIRMACACAO
}
