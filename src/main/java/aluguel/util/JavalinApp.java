package aluguel.util;

import aluguel.Controller;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private Javalin app =
            Javalin.create(config -> config.defaultContentType = "application/json")
                    .routes(() -> {
                        path("/ciclista", ()-> post(Controller::createCiclista));
                        path("/ciclista/:idCiclista", ()-> get(Controller::getCiclista));
                        path("/ciclista/:idCiclista", ()-> put(Controller::putCiclista));
                        path("/ciclista/:idCiclista", ()-> post(Controller::ativarCiclista));
                        path("/ciclista/existeEmail/:email", ()-> get(Controller::existeEmail));
                        path("/funcionario", ()-> post(Controller::createFuncionario));
                        path("/funcionario", ()-> get(Controller::getFuncionarios));
                        path("/funcionario/:idFuncionario", ()-> get(Controller::getFuncionario));
                        path("/funcionario/:idFuncionario", ()-> put(Controller::putFuncionario));
                        path("/funcionario/:idFuncionario", ()-> delete(Controller::deleteFuncionario));
                        path("/cartaoDeCredito/:idCiclista", ()-> get(Controller::getCartao));
                        path("/cartaoDeCredito/:idCiclista", ()-> put(Controller::putCartao));
                        path("/aluguel", ()-> post(Controller::alugar));
                        path("/devolver", ()-> post(Controller::devolver));
                    });



    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
