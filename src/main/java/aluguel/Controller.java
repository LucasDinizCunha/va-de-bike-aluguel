package aluguel;

import aluguel.models.*;
import com.google.gson.Gson;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.eclipse.jetty.util.ajax.JSON;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

public class Controller {
    public static ArrayList<Ciclista> ciclistas = new ArrayList<>();
    public static ArrayList<Funcionario> funcionarios = new ArrayList<>();
    public static ArrayList<Aluguel> alugueis = new ArrayList<>();
    private Controller() {}
    public static void createCiclista(Context ctx) {
        try {
        String cic = ctx.body();
        JSONObject request = new JSONObject(cic);

        JSONObject passaporte = new JSONObject(request.getJSONObject("ciclista").getJSONObject("passaporte"));

        Passaporte pass = new Passaporte (
                passaporte.getJSONObject("element").getString("numero"),
                passaporte.getJSONObject("element").getString("validade"),
                passaporte.getJSONObject("element").getString("pais")
        );

        JSONObject cartao = new JSONObject(request.getJSONObject("meioDePagamento"));

        CartaoDeCredito car = new CartaoDeCredito (
                cartao.getJSONObject("element").getString("nomeTitular"),
                cartao.getJSONObject("element").getString("numero"),
                cartao.getJSONObject("element").getString("validade"),
                cartao.getJSONObject("element").getString("cvv")
        );

        Ciclista cicl = new Ciclista (
                request.getJSONObject("ciclista").getString("nome"),
                request.getJSONObject("ciclista").getString("nascimento"),
                request.getJSONObject("ciclista").getString("cpf"),
                request.getJSONObject("ciclista").getString("nacionalidade"),
                request.getJSONObject("ciclista").getString("email"),
                request.getJSONObject("ciclista").getString("senha"),
                pass,
                car,
                StatusCiclista.AGUARDANDO_CONFIRMACACAO
        );

        ciclistas.add(cicl);

        ctx.status(200);
        ctx.html("CADASTRADO COM SUCESSO");
        ctx.json(JSON.parse(new Gson().toJson(cicl)));

        } catch (IllegalArgumentException e) {
            ctx.html("DADOS INVALIDOS");
            ctx.status(422);
        }
    }

    public static void getCiclista(Context ctx) {
        try {
            String cic = ctx.pathParamMap().get("idCiclista");

            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getId().equals(cic)) {
                    ctx.json(JSON.parse(new Gson().toJson(ciclista)));
                    ctx.status(200);
                }
            }

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void putCiclista(Context ctx) {
        try {
            String cic = ctx.body();
            JSONObject request = new JSONObject(cic);
            String id = ctx.pathParamMap().get("idCiclista");
            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getId().equals(id)) {

                    ciclista.setNome(request.getString("nome"));
                    ciclista.setNascimento(request.getString("nascimento"));
                    ciclista.setCpf(request.getString("cpf"));
                    ciclista.setNacionalidade(request.getString("nacionalidade"));
                    ciclista.setEmail(request.getString("email"));
                    ciclista.getPassaporte().setNumero(request.getJSONObject("passaporte").getString("numero"));
                    ciclista.getPassaporte().setValidade(request.getJSONObject("passaporte").getString("validade"));
                    ciclista.getPassaporte().setPais(request.getJSONObject("passaporte").getString("pais"));


                    ctx.json(JSON.parse(new Gson().toJson(ciclista)));
                    ctx.status(200);
                }
            }

            ctx.status(200);

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void ativarCiclista(Context ctx) {
        try {
            String cic = ctx.pathParamMap().get("idCiclista");

            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getId().equals(cic)) {
                    if(!ciclista.getStatus().equals("ATIVO")) {
                        ciclista.setStatus(StatusCiclista.ATIVO);
                    }

                    ctx.json(JSON.parse(new Gson().toJson(ciclista)));
                    ctx.status(200);
                }
            }

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void existeEmail(Context ctx) {
        try {
            String email = ctx.pathParamMap().get("email");

            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getEmail().equals(email)) {
                    ctx.html("True");
                }
            }
            ctx.status(200);

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void createFuncionario(Context ctx) {
        try {
            String fun = ctx.body();
            JSONObject request = new JSONObject(fun);

            Funcionario func = new Funcionario (
                    request.getString("senha"),
                    request.getString("email"),
                    request.getString("nome"),
                    Integer.parseInt(request.getString("idade")),
                    request.getString("funcao"),
                    request.getString("cpf")
            );

            funcionarios.add(func);

            ctx.status(200);
            ctx.html("CADASTRADO COM SUCESSO");
            ctx.json(JSON.parse(new Gson().toJson(func)));

        } catch (IllegalArgumentException e) {
            ctx.html("DADOS INVALIDOS");
            ctx.status(422);
        }
    }

    public static void getFuncionarios(Context ctx) {
        try {
            ctx.json(JSON.parse(new Gson().toJson(funcionarios)));
            ctx.status(200);

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void getFuncionario(Context ctx) {
        try {
            String fun = ctx.pathParamMap().get("idFuncionario");

            for (Funcionario funcionario : funcionarios) {
                if (funcionario.getMatricula().equals(fun)) {
                    ctx.json(JSON.parse(new Gson().toJson(funcionario)));
                    ctx.status(200);
                }
            }

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void putFuncionario(Context ctx) {
        try {
            String fun = ctx.body();
            JSONObject request = new JSONObject(fun);
            String id = ctx.pathParamMap().get("idFuncionario");
            for (Funcionario funcionario : funcionarios) {
                if (funcionario.getMatricula().equals(id)) {

                    funcionario.setNome(request.getString("nome"));
                    funcionario.setFuncao(request.getString("funcao"));
                    funcionario.setCpf(request.getString("cpf"));
                    funcionario.setIdade(Integer.parseInt(request.getString("idade")));
                    funcionario.setEmail(request.getString("email"));
                    funcionario.setSenha(request.getString("senha"));

                    ctx.json(JSON.parse(new Gson().toJson(funcionario)));
                    ctx.status(200);
                }
            }

            ctx.status(200);

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void deleteFuncionario(Context ctx) {
        try {
            String fun = ctx.pathParamMap().get("idFuncionario");

            for (Funcionario funcionario : funcionarios) {
                if (funcionario.getMatricula().equals(fun)) {

                    funcionarios.remove(funcionario);
                    ctx.html("Funcionario removido com sucesso");
                    ctx.status(200);
                    break;
                }
            }

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void getCartao(Context ctx) {
        try {
            String cic = ctx.pathParamMap().get("idCiclista");

            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getId().equals(cic)) {
                    ctx.json(JSON.parse(new Gson().toJson(ciclista.getCartaoDeCredito())));
                    ctx.status(200);
                }
            }

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void putCartao(Context ctx) {
        try {
            String cic = ctx.body();
            JSONObject request = new JSONObject(cic);
            String id = ctx.pathParamMap().get("idCiclista");
            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getId().equals(id)) {

                    ciclista.getCartaoDeCredito().setCvv(request.getString("cvv"));
                    ciclista.getCartaoDeCredito().setNumero(request.getString("numero"));
                    ciclista.getCartaoDeCredito().setValidade(request.getString("validade"));
                    ciclista.getCartaoDeCredito().setNomeTitular(request.getString("nomeTitular"));



                    ctx.json(JSON.parse(new Gson().toJson(ciclista.getCartaoDeCredito())));
                    ctx.status(200);
                }
            }
            ctx.status(200);

        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void alugar(Context ctx) {
        try {
            String cic = ctx.body();
            JSONObject request = new JSONObject(cic);
            // INTEGRACAO COM EQUIPAMENTO IMPLEMENTADA COM SUCESSO!!!!!!
            HttpResponse<String> tranca = Unirest.get("https://va-de-bicicleta-equipamento.herokuapp.com/tranca/" + request.getString("trancaInicio")).asString();
            JSONObject tr = new JSONObject(tranca.getBody());

            for (Ciclista ciclista : ciclistas) {
                if (ciclista.getId().equals(request.getString("ciclista")) && tr.getString("status").equals("LIVRE")) {
                    // NAO TO CHAMANDO O VALIDAR CARTAO POIS NAO ESTA IMPLEMENTADO NO EXTERNO!!!!!!
                    // if(validarCartao(ciclista.getCartaoDeCredito())) {
                        Aluguel al = new Aluguel (
                                // TRANCA RETORNANDO COM ID BICICLETA NULL NO EQUIPAMENTO, NAO FOI RESOLVIDO ATE A HORA DE GRAVAR O VIDEO
                                // tr.getString("bicicleta"),
                                "teste",
                                new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()),
                                request.getString("trancaInicio"),
                                "null",
                                "null",
                                "null",
                                ciclista.getId()
                        );
                        alugueis.add(al);

                        ctx.json(JSON.parse(new Gson().toJson(al)));
                        ctx.status(200);
                    // }
                }
            }
        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static void devolver(Context ctx) {
        try {
            String cic = ctx.body();
            JSONObject request = new JSONObject(cic);

            for(Aluguel aluguel : alugueis) {
                if(aluguel.getBicicletaId().equals(request.getString("idBicicleta"))) {
                    // INTEGRACAO NAO IMPLEMENTADO NO EXTERNO!!!!!!
                    //  String str = String.format("{\"valor\":10,\"ciclista\":\"%s\"}", aluguel.getCiclistaId());
                    //  HttpResponse<String> cobranca = Unirest.post("https://teamkrypto-vadebicicleta.herokuapp.com/cobranca").body(str).asString();
                    aluguel.setHoraFim(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()));
                    aluguel.setTrancaFimId(request.getString("idTranca"));
                    //  aluguel.setCobrancaId(cobranca.getString("Id"));
                    aluguel.setCobrancaId(UUID.randomUUID().toString());

                    ctx.json(JSON.parse(new Gson().toJson(aluguel)));
                    ctx.status(200);
                }
            }
        } catch (IllegalArgumentException e) {
            ctx.html("DADOSINVALIDOS");
            ctx.status(422);
        }
    }

    public static boolean validarCartao(CartaoDeCredito card) {
        // INTEGRACAO NAO IMPLEMENTADO NO EXTERNO!!!!!!

        HttpResponse<String> response = Unirest.post("https://teamkrypto-vadebicicleta.herokuapp.com/validaCartaoDeCredito")
                .body(card).asString();
        return response.toString().equals("true") ? true : false;
    }

    public static void addCic(Ciclista cic) {
        ciclistas.add(cic);
    }
    public static void addFun(Funcionario fun) {
        funcionarios.add(fun);
    }
    public static void addAl(Aluguel al) {
        alugueis.add(al);
    }
}
