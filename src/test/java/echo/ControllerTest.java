package echo;

import static org.junit.jupiter.api.Assertions.*;
import aluguel.Controller;
import aluguel.models.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import aluguel.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void createCiclistaTest() {
        HttpResponse<String> response = Unirest.post("http://localhost:7010/ciclista")
                .body(
                        "{\"ciclista\":{\"nome\":\"string\",\"nascimento\":\"2022-07-27\",\"cpf\":\"26001958934\",\"passaporte\":{\"numero\":\"string\",\"validade\":\"2022-07-27\",\"pais\":\"RA\"},\"nacionalidade\":\"string\",\"email\":\"user@example.com\",\"senha\":\"string\"},\"meioDePagamento\":{\"nomeTitular\":\"string\",\"numero\":\"9229749045268285175245311\",\"validade\":\"2022-07-27\",\"cvv\":\"4718\"}}"
                ).asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void getCiclistaTest() {
        Ciclista c = new Ciclista("string", "2022-07-27","37011785229", "string", "user@example.com", "a",new Passaporte("string", "2022-07-27", "ZX"),new CartaoDeCredito("a", "a", "a", "a"), StatusCiclista.AGUARDANDO_CONFIRMACACAO);
        c.setId("1");
        Controller.addCic(c);

        HttpResponse<String> response = Unirest.get("http://localhost:7010/ciclista/1").asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void putCiclistaTest() {
        Ciclista c = new Ciclista("nome", "a","a", "a", "a", "a",new Passaporte("a", "a", "a"),new CartaoDeCredito("a", "a", "a", "a"), StatusCiclista.AGUARDANDO_CONFIRMACACAO);
        c.setId("1");
        Controller.addCic(c);
        HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/1")
                .body(
                        "{\"id\":\"1\",\"status\":\"string\",\"nome\":\"string\",\"nascimento\":\"2022-07-27\",\"cpf\":\"37011785229\",\"passaporte\":{\"numero\":\"string\",\"validade\":\"2022-07-27\",\"pais\":\"ZX\"},\"nacionalidade\":\"string\",\"email\":\"user@example.com\"}"
                ).asString();

        String resultadoEsperado = "{\"cartaoDeCredito\":{\"cvv\":\"a\",\"numero\":\"a\",\"nomeTitular\":\"a\",\"validade\":\"a\"},\"senha\":\"a\",\"nascimento\":\"2022-07-27\",\"passaporte\":{\"numero\":\"string\",\"validade\":\"2022-07-27\",\"pais\":\"ZX\"},\"cpf\":\"37011785229\",\"nome\":\"string\",\"id\":\"1\",\"nacionalidade\":\"string\",\"email\":\"user@example.com\",\"status\":\"AGUARDANDO_CONFIRMACACAO\"}";
        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void ativarCiclistaTest() {
        Ciclista c = new Ciclista("nome", "a","a", "a", "a", "a",new Passaporte("a", "a", "a"),new CartaoDeCredito("a", "a", "a", "a"), StatusCiclista.AGUARDANDO_CONFIRMACACAO);
        c.setId("1");
        Controller.addCic(c);
        HttpResponse<String> response = Unirest.post("http://localhost:7010/ciclista/1").asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void createFuncionarioTest() {
        HttpResponse<String> response = Unirest.post("http://localhost:7010/funcionario")
                .body(
                        "{\"senha\":\"string\",\"email\":\"user@example.com\",\"nome\":\"string\",\"idade\":0,\"funcao\":\"string\",\"cpf\":\"string\"}"
                ).asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void getFuncionariosTest() {
        Funcionario f = new Funcionario("a", "a","a", 1, "a", "a");
        f.setMatricula("1");
        Controller.addFun(f);

        HttpResponse<String> response = Unirest.get("http://localhost:7010/funcionario").asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void getFuncionarioTest() {
        Funcionario f = new Funcionario("a", "a","a", 1, "a", "a");
        f.setMatricula("99");
        Controller.addFun(f);

        HttpResponse<String> response = Unirest.get("http://localhost:7010/funcionario/99").asString();

        String resultadoEsperado = "{\"funcao\":\"a\",\"senha\":\"a\",\"idade\":1,\"matricula\":\"99\",\"cpf\":\"a\",\"nome\":\"a\",\"email\":\"a\"}";
        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void putFuncionarioTest() {
        Funcionario f = new Funcionario("b", "a","a", 1, "a", "a");
        f.setMatricula("12");
        Controller.addFun(f);

        HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/12").body(
                "{\"senha\":\"string\",\"email\":\"user@example.com\",\"nome\":\"string\",\"idade\":0,\"funcao\":\"string\",\"cpf\":\"string\"}"
        ).asString();

        String resultadoEsperado = "{\"funcao\":\"string\",\"senha\":\"string\",\"idade\":0,\"matricula\":\"12\",\"cpf\":\"string\",\"nome\":\"string\",\"email\":\"user@example.com\"}";
        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void deleteFuncionarioTest() {
        Funcionario f = new Funcionario("a", "a","a", 1, "a", "a");
        f.setMatricula("1");
        Controller.addFun(f);

        HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/1").asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void getCartaoTest() {
        Ciclista c = new Ciclista("nome", "a","a", "a", "a", "a",new Passaporte("a", "a", "a"),new CartaoDeCredito("string", "6146161220518637972085882062696863385491650946137034134268694427804613938214295996087281422673419", "2022-07-27", "110"), StatusCiclista.AGUARDANDO_CONFIRMACACAO);
        c.setId("3333");
        Controller.addCic(c);

        HttpResponse<String> response = Unirest.get("http://localhost:7010/cartaoDeCredito/3333").asString();

        String resultadoEsperado = "{\"cvv\":\"110\",\"numero\":\"6146161220518637972085882062696863385491650946137034134268694427804613938214295996087281422673419\",\"nomeTitular\":\"string\",\"validade\":\"2022-07-27\"}";
        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void putCartaoTest() {
        Ciclista c = new Ciclista("nome", "a","a", "a", "a", "a",new Passaporte("a", "a", "a"),new CartaoDeCredito("a", "a", "a", "a"), StatusCiclista.AGUARDANDO_CONFIRMACACAO);
        c.setId("1");
        Controller.addCic(c);

        HttpResponse<String> response = Unirest.put("http://localhost:7010/cartaoDeCredito/1").body(
                "{\"nomeTitular\":\"string\",\"numero\":\"6146161220518637972085882062696863385491650946137034134268694427804613938214295996087281422673419\",\"validade\":\"2022-07-27\",\"cvv\":\"110\"}"
        ).asString();

        String resultadoEsperado = "{\"cvv\":\"110\",\"numero\":\"6146161220518637972085882062696863385491650946137034134268694427804613938214295996087281422673419\",\"nomeTitular\":\"string\",\"validade\":\"2022-07-27\"}";
        assertEquals(200, response.getStatus());
        assert(response.getBody().contains(resultadoEsperado));
    }

    @Test
    void aluguelTest() {
        Ciclista c = new Ciclista("nome", "a","a", "a", "a", "a",new Passaporte("a", "a", "a"),new CartaoDeCredito("a", "a", "a", "a"), StatusCiclista.AGUARDANDO_CONFIRMACACAO);
        c.setId("4444");
        Controller.addCic(c);
        HttpResponse<String> response = Unirest.post("http://localhost:7010/aluguel")
                .body(
                        "{\"ciclista\":\"4444\",\"trancaInicio\":\"a2842804-aeea-40de-aae7-7157e4e06460\"}"
                ).asString();

        assertEquals(200, response.getStatus());
    }

    @Test
    void devolverTest() {
        Aluguel a = new Aluguel("teste", "a","a", "a", "a", "a", "a");
        Controller.addAl(a);
        HttpResponse<String> response = Unirest.post("http://localhost:7010/devolver")
                .body(
                        "{\"idTranca\":\"a2842804-aeea-40de-aae7-7157e4e06460\",\"idBicicleta\":\"teste\"}"
                ).asString();

        assertEquals(200, response.getStatus());
        assert(response.getBody().contains("bicicletaId\":\"teste"));
        assert(response.getBody().contains("trancaFimId\":\"a2842804-aeea-40de-aae7-7157e4e06460"));
    }
}
